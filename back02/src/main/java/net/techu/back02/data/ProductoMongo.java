package net.techu.back02.data;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("Back02-C805714Test")
public class ProductoMongo {

    @Id
    public String id;
    public String descripcion;
    public String precio;

    public ProductoMongo(){
    }

    public ProductoMongo(String descripcion, String precio) {
        this.descripcion= descripcion;
        this.precio= precio; }

    @Override
    public String toString(){
        return String.format("Producto [id=%s,descripcion=%s,precio=%s]", id,descripcion, precio); }

}
