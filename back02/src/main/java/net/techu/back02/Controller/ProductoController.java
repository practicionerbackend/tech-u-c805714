package net.techu.back02.Controller;

import net.techu.back02.data.ProductoMongo;
import net.techu.back02.data.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductoController {

    @Autowired
    private ProductoRepository repository;

    //Metodo para obtener todos los productos
    @GetMapping(value = "/Productos", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> getProducts() {
        List<ProductoMongo> list = repository.findAll();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    //Metodo para obtener el producto por Id
    @GetMapping(value = "/Productos/{id}", produces = "application/json")
    public ResponseEntity<Optional<ProductoMongo>> getProductsId(@PathVariable String id) {
        Optional<ProductoMongo> list= repository.findById(id);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    //Metodo para incluir producto
    @PostMapping(value = "/Productos")
    public ResponseEntity<String> addProducto(@RequestBody ProductoMongo productoMongo) {
        ProductoMongo resultado = repository.insert(productoMongo);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }

    //Metodo para modificar el nombre del producto por Id
    @PutMapping(value = "/Productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable String id, @RequestBody ProductoMongo productoMongo) {
        Optional<ProductoMongo> result = repository.findById(id);
        if (result.isPresent()) {
            ProductoMongo productoAModificar = result.get();
            productoAModificar.descripcion = productoMongo.descripcion;
            productoAModificar.precio = productoMongo.precio;
            ProductoMongo save = repository.save(result.get());
            return new ResponseEntity<String>(result.toString(), HttpStatus.OK);
        }
        return new ResponseEntity<String>("Producto no encontrado", HttpStatus.NOT_FOUND);
    }

    //Metodo para eliminar todos los Productos
    @DeleteMapping(value = "/Productos/")
    public ResponseEntity<String> deleteProducto() {
        repository.deleteAll();
        return new ResponseEntity<String>("Producto borrado", HttpStatus.OK);
    }

    //Metodo para eliminar productos por Id
    @DeleteMapping(value = "/Productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable String id) {
        repository.deleteById(id);
        return new ResponseEntity<String>("Producto borrado", HttpStatus.OK);
    }
}
