package net.techu.back01.controller;

import net.techu.back01.data.Producto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductoController {

    private ArrayList<Producto> listProducts;
    public ProductoController() {
        listProducts = new ArrayList<>();
        listProducts.add(new Producto(1,"PR1", 18.35));
        listProducts.add(new Producto(2,"PR2", 29.33));
        listProducts.add(new Producto(3,"PR3", 37.00));
        listProducts.add(new Producto(4,"PR4", 46.99));
        listProducts.add(new Producto(5,"PR5", 54.85));
        listProducts.add(new Producto(6,"PR6", 65.27)); }

    //Metodo para obtener todos los productos
    @GetMapping(value="/Productos",produces="application/json")
    public ResponseEntity<List<Producto>> getProducts() {
        return new ResponseEntity<>(listProducts, HttpStatus.OK);}

    //Metodo para obtener el producto por Id
    @GetMapping(value="/Productos/{id}",produces="application/json")
    public ResponseEntity<Producto> getProductsId(@PathVariable int id) {
        Producto productsId =null;
        ResponseEntity<Producto> response= null;
        try {
            productsId= listProducts.get(id);
            response= new ResponseEntity<>(productsId, HttpStatus.OK); }
        catch (Exception ex) {
            response= new ResponseEntity<>(productsId, HttpStatus.NOT_FOUND); }
        return response; }

    //Metodo para incluir producto predefinido
    @PostMapping(value="/Productos/PR3")
    public ResponseEntity<String> addProducto() {
        listProducts.add(new Producto(99,"PR3",100.55));
        return new ResponseEntity<>("Producto Creado Correctamente", HttpStatus.CREATED);
    }

    //Metodo para incluir producto
    @PostMapping(value="/Productos")
    public ResponseEntity<String> addProductName(@RequestBody Producto newProduct){
        listProducts.add(newProduct);
        return new ResponseEntity<>("Producto Creado Correctamente", HttpStatus.CREATED);
    }

    //Metodo para modificar el nombre del producto por Id
    @PatchMapping(value="/Productos/{id}")
    public ResponseEntity <String> patchProduct(@PathVariable int id, @RequestBody Producto modify){
        ResponseEntity <String> response= null;
        try {
            Producto productoAModificar = listProducts.get(id);
            productoAModificar.setNombre(modify.getNombre());
            listProducts.set(id, productoAModificar);
            response = new ResponseEntity<>(HttpStatus.ACCEPTED); }
        catch (Exception ex) {
            response= new ResponseEntity<>("No se ah encontrado el producto", HttpStatus.NOT_FOUND);
        } return response;
    }

    //Metodo para modificar el productos por Id
    @PutMapping(value="/Productos/{id}")
    public ResponseEntity <String> putProduct(@PathVariable int id, @RequestBody Producto modify){
        ResponseEntity <String> response= null;
        try {
            Producto productoAModificar = listProducts.get(id);
            productoAModificar.setNombre(modify.getNombre());
            productoAModificar.setPrecio(modify.getPrecio());
            listProducts.set(id, productoAModificar);
            response = new ResponseEntity<>(HttpStatus.ACCEPTED); }
        catch (Exception ex) {
            response= new ResponseEntity<>("No se ah encontrado el producto", HttpStatus.NOT_FOUND);
        } return response;
    }

    //Metodo para modificar el precio de todos los productos
    @PutMapping("/Productos")
    public ResponseEntity<String> modifyPrecio()    {
        ResponseEntity<String> resultado = null;
        for (Producto p: listProducts) {
            p.setPrecio(p.getPrecio()*1.25); }
        resultado = new ResponseEntity<>(HttpStatus.ACCEPTED);
        return resultado; }

    //Metodo para eliminar todos los Productos
    @DeleteMapping(value="/Productos/")
    public ResponseEntity <List<Producto>> deleteProducto(){
        listProducts.removeAll(listProducts);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    //Metodo para eliminar productos por Id
    @DeleteMapping(value="/Productos/{id}")
    public ResponseEntity <String> deleteProductoId(@PathVariable int id){
        ResponseEntity <String> response;
        try {
            listProducts.remove(id);
            response = new ResponseEntity<>(HttpStatus.NO_CONTENT); }
        catch (Exception ex) {
            response= new ResponseEntity<>("No se ah encontrado el producto", HttpStatus.NOT_FOUND); }
        return response; }

}
